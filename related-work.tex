% !TeX spellcheck = en_US
\chapter{Related Work}
\label{cha:related-work}
There has been a range of work conducted to address the syntactic- and semantic-based integration approaches.
Each work focused on identifying the gaps and limitations in each approach and testing the suggested methodology on a practical example.
The research conducted by Fadi Jabbour focused on the syntactic-level by using string matching techniques to achieve a semi-automated model transformation.
Yong Wang et al.\ focused on the semantic-level by using ontologies to achieve an automatic adaptation of component interfaces.

This chapter reviews both research and the approaches to tackle the problems encountered in their work, which will help to identify the gaps that make it possible to join both approaches.
\section{Model Transformation for Domain Specific Architecture Languages in the Automotive Software Development}
\label{sec:model-transformation-for-domain-specific-architecture-languages-in-the-automotive-software-development}
The research work of Fadi Jabbour emerged to address the increase in the complexity of the software systems.
It is the result of the growing number of software systems that need to interact together.
Using a standardized architecture description languages is the first step towards achieving interoperability among integration tools, and therefore, approach this problem.
The fact that software components are developed by different parties leads to a different representation of the models even in case of modeling the same domain.
As for an example, \acp{adl} is a description language that belongs to the same application domain and shares the same semantics; however, architectures use different notations for describing the same software architecture.

The research work contributed to finding a solution for achieving interoperability between different \acp{adl} by developing an automated transformation mechanism that gives the user the ability to evaluate the result obtained by the transformation.

The work encountered integration gaps between \acp{adl}, which can be categorized into three different levels, ordered from the least to the most abstracted.
First, the technology level, which refers to the technologies used to build the \acp{adl}, such as \ac{xml}, and \ac{xmi}.
Second, the modeling language level, which refers to the existence of different syntaxes.
Third, the structure level, which refers to the existence of different characteristics of the model.
The integration tool needed to address and solve those challenges by implementing a tool that is compatible with the commonly used technologies and using a well-defined mapping between the model structures such as the weaving model and model transformation.

Figure \ref{fig:model-weaving-according-to-jabbour} shows the mode of operation for the proposed model weaving as part of the model transformation concept.
The model weaving produces a mapping between the source and target metamodel, which can be used for transforming the model using the model matching procedure.

The study introduced a system with two components: the reusable transformation rules, and the improved model weaving operation.
The idea behind the reusable transformation rules is to define transformation rules to help to transform and therefore match the source and target models.
The idea behind the improved model weaving operation is to give the user the ability to create relations between source and target metamodels.
The overall approach can be further improved by applying the weaving model on smaller logical parts by matching sub-parts of the source and target metamodels, which can be combined to create the overall model transformation.

The matching algorithm, which generates the weaving model, operates on all elements of the source and target metamodels (e.g., classes, attributes, references).
Using all structure information from the model is a well-known approach, as presented in chapter \ref{cha:background}.
The developer can update and correct the weaving result.

While the primary goal of the study was to provide an automatic approach for the model transformation, but the results of the model weaving were not satisfying, since the element names are abbreviations that consist of the first letter of each word.
In many cases, the attributes share the same name \textsf{``value''} and type \textsf{``string''} with no size limit.
Therefore, the system is not applicable in a real industrial environment, because errors that occur in the early stages of the algorithm will propagate in later stages as well.
Thus, the effort needed to adjust the results is more significant than constructing the weaving model from scratch.

Therefore, the idea to obtain an automatic approach was replaced by a semi-automatic multi-steps algorithm that requires the developer's interaction, which resulted in correcting errors at each stage of the algorithm and preventing their propagation.
The algorithm starts by matching classes of source and target metamodels based on their internal structure such as name, size, multiplicity, and similarity of attributes.
The developer then validates and corrects the results.
The algorithm then searches for equivalent properties such as name, type, and lower and upper bound in the equivalent classes found in the previous matching step.
The developer once again validates and corrects the results.

The work was concluded by providing future research suggestions, for example, achieving bidirectional transformation.
The research investigated the creation of transformation only from source to target models.
However, It would be beneficial to construct a bidirectional transformation mechanism, also from the target back to the source model.
\newline\noindent
\cite{jabbour_model_2015}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/related-work/model-weaving}}
	\caption{Model weaving, according to Jabbour \cite[p.\ 21]{jabbour_model_2015}}
	\label{fig:model-weaving-according-to-jabbour}
\end{figure}
\section{Ontology-based Automatic Adaptation of Component Interfaces in Dynamic Adaptive Systems}
\label{sec:ontology-based-automatic-adaptation-of-component-interfaces-in-dynamic-adaptive-systems}
The research work of Yong Wang et al.\ emerged to address the reusability of components in a new application context using semantic models.
The research reviewed dynamic adaptive systems that change their behavior at runtime based on changes in the users' requirements or system's environment.
\ac{daisi} is an example of a component-based dynamic adaptive system.
\ac{daisi}'s components are defined using an interface that describes required and provided services.

The research work contributed to finding a solution for reusing components in application contexts outside the scope of their intended implementation.
The goal is to couple provided and required services that are semantically compatible but syntactically not.
Therefore, three types of incompatibility are covered: different naming, different data structure, and different control structure.
The three types of incompatibility are shown in figure \ref{fig:taxonomy-of-incompatibilities-between-interfaces-according-to-wang-et-al}
The different naming is the case where names of interfaces or the functions do not match but share the same semantics.
The different data structure is the case where the parameters differ in their data types; however, the encapsulated data can be mapped to each other.
The different control structure is the case where different interfaces provide information which can be composed rather than mapped.

The study introduced the usage of a central ontology as the common knowledge base, which provides the mapping between the semantically compatible provided and required services.
The approach extends the \ac{daisi} components' interfaces through developing an ontology-based adapter as shown in figure \ref{fig:three-layer-ontology-structure-according-to-wang-et-al}
The central ontology consists of three layers: upper layer, application layer, and interface layer.
The upper layer's ontology is called \textsf{UpperOntology}, which contains basic knowledge definition.
The application layer's ontology contains all definitions that are relevant for an application.
The interface layer represents the domain interfaces with their names, methods, parameters, and data types.

Figure \ref{fig:process-for-mapping-required-interfaces-to-provided-interfaces-according-to-wang-et-al} shows the mode of operation for the proposed system.
First, the information collector transforms the annotations in the interface definition.
Then, a mapper search in all instances of the ontology to find a mapping between required interfaces that can be used by provided interfaces.
Finally, the adapter component is created to link the mapped interfaces regarding one of the incompatibilities presented earlier.

The proposed approach has the advantage of making every part of the ontologies and interfaces being developed separately.
Thus, opening further research in the field of ontology mapping and merging so that layers can be merged with other dynamic adaptive system domains.

The work was concluded by providing further research suggestions, for example, the usage of distributed ontologies, so that every component is directly linked to an ontology which describes its structure.
\newline\noindent
\cite{wang_ontology_2016}
\begin{figure}[h]
	\centering
	\frame{\includegraphics{./figure/related-work/taxonomy-of-incompatibilities-between-interfaces}}
	\caption{Taxonomy of incompatibilities between interfaces, according to Wang et al.\ \cite[p.\ 2]{wang_ontology_2016}}
	\label{fig:taxonomy-of-incompatibilities-between-interfaces-according-to-wang-et-al}
\end{figure}
\begin{figure}[h]
	\centering
	\frame{\includegraphics{./figure/related-work/three-layer-ontology-structure}}
	\caption{Three-layer ontology structure, according to Wang et al.\ \cite[p.\ 5]{wang_ontology_2016}}
	\label{fig:three-layer-ontology-structure-according-to-wang-et-al}
\end{figure}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/related-work/process-for-mapping-required-interfaces-to-provided-interfaces}}
	\caption{Process for mapping required interfaces to provided interfaces, according to Wang et al.\ \cite[p.\ 6]{wang_ontology_2016}}
	\label{fig:process-for-mapping-required-interfaces-to-provided-interfaces-according-to-wang-et-al}
\end{figure}






%===================================================
%\section{Ontology-based Automatic Adaptation of Component Interfaces in Dynamic Adaptive Systems}
%goal:
%couple provided and required services that are syntactically incompatible, however on a semantic level offer the needed data or operations \cite[p.\ 1]{wang_ontology_2016}.
%construct an adapter that enables interoperability between services that are compatible a semantic level \cite[p.\ 1]{wang_ontology_2016}.
%using a central ontology as the common knowledge base \cite[p.\ 1]{wang_ontology_2016}.
%extend the \ac{daisi} infrastructure by an ontology-based adapter engine for service adaptation \cite[p.\ 1]{wang_ontology_2016}.

%when merging different dynamic adaptive system instances, their upper ontologies can be merged \cite[p.\ 5]{wang_ontology_2016}.

%===================================================
%\section{Introduction to Data Mining}
%\label{sec:introduction-to-data-mining}

%\section{Principles of Data Mining}
%\label{sec:principles-of-data-mining}

%\subsection{Data Labeling}
%\label{subsec:data-labeling}

%\subsection{Data Preparation}
%\label{subsec:data-preparation}

%\subsection{Dealing with Missing Values}
%\label{subsec:dealing-with-missing-values}

%\subsection{Dealing with Noisy Data}
%\label{subsec:dealing-with-noisy-data}

%\subsection{Data Reduction}
%\label{subsec:data-reduction}

%\subsection{Feature Selection}
%\label{subsec:feature-selection}

%\subsection{Instance Selection}
%\label{subsec:instance-selection}

%\section{Introduction to Data Integration}
%\label{sec:introduction-to-data-integration}

%\section{Principles of Data Integration}
%\label{sec:principles-of-data-integration}

%\subsection{String Matching}
%\label{subsec:string-matching}

%\subsection{General Schema Manipulation}
%\label{subsec:general-schema-manipulation}

%\subsection{Data Matching}
%\label{subsec:data-matching}

%\subsection{Data Warehousing}
%\label{subsec:data-warehousing}

%\section{Dataspaces for Information Management}
%\label{sec:dataspaces-for-information-management}

%\section{Polystore Database Management Systems}
%\label{sec:polystore-database-management-systems}