% !TeX spellcheck = en_US
\chapter{Approach}
\label{cha:approach}
As stated before, the goal of this work is to introduce a domain knowledge life cycle and a toolchain to help the creation and development of domain representations on semantic bases.
The life cycle consists of four procedures: composition, transformation, matching, and merging.
The following chapter outlines each procedure.
Then compare existing approaches and tools with the help of evaluation matrices, to compose the toolchain.

\section{Ontology Composition Procedure}
\label{sec:ontology-composition-procedure}
An Ontology is a specification of an abstract and simplified view of a particular topic.
It consists of a set of vocabularies and axioms to describe a specific domain.
\ac{owl} 2 is an ontology language for the semantic web.
The structure of the \ac{owl} 2 language, as well as its components and their relations, are described in figures \ref{fig:structure-of-owl-2-ontologies} and \ref{fig:parts-hierarchy-of-the-owl-2-rdf-based-semantics}.
The following subsections describe each component of the \ac{owl} 2 language.
\newline\noindent
\textbf{\ac{iri}} is an internet protocol standard that extends the \ac{uri} by using the universal character set, instead of the \ac{ascii}.
\acp{iri} are used to identify ontologies and their versions.
Additionally, the elements of an ontology may also be identified using \acp{iri}.
\newline\noindent
\textbf{Annotations} are used to associate information with an ontology, e.g., description field or current version.
\begin{CustVerbatim}
	owl:backwardCompatibleWith( <http://www.w3.org/2019/Example> )
\end{CustVerbatim}
\textbf{Axioms} are the main component of an ontology and used to describe truth inside the domain.
They can be declarations or about classes, objects, or data.
\newline\noindent
\textbf{Classes} describe sets of individuals.
For example, the classes \textsf{\small{a:Teacher}} and \textsf{\small{a:Person}} can be used to represent the set of all teachers and persons, respectively.
\begin{CustVerbatim}
	SubClassOf( a:Teacher a:Person )
	\normalfont{Each teacher is a person}
\end{CustVerbatim}
\textbf{Datatypes} are the same as classes, with only the difference that they refer to sets of data values instead of individuals.
For example, the datatype \textsf{\small{xsd:integer}} denotes the set of all integers and can be used as axioms.
\begin{CustVerbatim}
	DataPropertyRange( a:hasAge xsd:integer )
	\normalfont{The range of the \textsf{a:hasAge} data property is \textsf{xsd:integer}}
\end{CustVerbatim}
\textbf{Object properties} connect pairs of individuals.
For example, the object property \textsf{\small{a:teachesSubject}} can be used to present the parenthood relationship between individuals.
\begin{CustVerbatim}
	ObjectPropertyAssertion( a:teachesSubject a:Alan a:CS101 )
	\normalfont{Alan teaches CS101 subject}
\end{CustVerbatim}
\textbf{Data properties} are the same as object properties, with only the difference that they connect individuals to literals instead of to one another.
For example, the data property \textsf{\small{a:hasName}} can be used to associate a name to a class.
\begin{CustVerbatim}
	DataPropertyAssertion( a:hasName a:Alan "Alan Turing" )
	\normalfont{Alan's name is "Alan Turing"}
\end{CustVerbatim}
\textbf{Annotation properties} provide annotations to an ontology, axiom, or an \ac{iri}.
For example, the annotation property \textsf{\small{rdfs:comment}} displays additional information.
\begin{CustVerbatim}
	AnnotationAssertion( rdfs:comment a:Alan "Alan was a mathematician" )
	\normalfont{This axiom provides a comment for \textsf{a:Alan}}
\end{CustVerbatim}
\textbf{Individuals} represent objects from a domain.
Individuals are given an explicit name that is used to refer to the same object.
Anonymous individuals can not be accessed globally and are thus local to the ontology containing its instantiation.
\begin{CustVerbatim}
	ClassAssertion( a:Teacher a:Alan )
	\normalfont{Alan is a teacher}
\end{CustVerbatim}
\textbf{Literals} represent data values such as string, decimal, or integer.
It consists of a lexical form denoting its value and a datatype.
\begin{CustVerbatim}
	"1"\^{}\^{}xsd:integer
	\normalfont{A literal that represents the integer 1}
\end{CustVerbatim}
\noindent
\cite{abburu_survey_2013, schneider_owl_20012, motik_owl_2012, gasevic_model_2009, hendler_agents_2001, gruber_translation_1993}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/structure-of-owl-2-ontologies}}
	\caption{Structure of \acs{owl} 2 ontologies, according to Motik et al.\ \cite[p.\ 8]{motik_owl_2012}}
	\label{fig:structure-of-owl-2-ontologies}
\end{figure}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/parts-hierarchy-of-the-owl-2-rdf-based-semantics}}
	\caption{Parts hierarchy of the \acs{owl} 2 \acs{rdf}-based semantics, according to Schneider et al.\ \cite[p.\ 6]{schneider_owl_20012}}
	\label{fig:parts-hierarchy-of-the-owl-2-rdf-based-semantics}
\end{figure}
\section{Data Source Transformation Procedure}
\label{sec:data-source-transformation-procedure}
As described in section \ref{subsec:ontology-learning}, the ontology learning process is the first component in a semantic-based integration process.
In this context, we describe it as a data source transformation process.
The procedure extracts the vocabulary and data which describes the domain.
This vocabulary may be presented in a schema description language as in \ac{xsd} or a tabular header as in \ac{csv}.
The extracted vocabulary and data are then inserted into a knowledge base.
It is beneficial to separate knowledge representation from procedural aspects related to its application in order to reuse the knowledge across other systems.

Despite its benefits, the procedure also encounters some limitations.
First, the extracted vocabulary is based on the assumptions of the domain expert constructing it.
Hence, the presence of the domain expert is required to approve the validation of the extracted knowledge.
Second, the construction of ontologies is costly due to the trade-off between a large amount of knowledge, and a provision of abstraction to enhance its reusability.
Therefore, the correctness and consistency can not be guaranteed.

The traditional ontology learning builds on natural language processing and machine learning, using methods such as named entity recognition, anaphora resolution, and morphology components.
The named entity recognition method extracts a small number of classes from domain-specific collections of unstructured texts.
In the scope of this study, such techniques are not required, as the extraction is based on structured data sources.
It is sufficient to transform the structured data into a knowledge representation.
\newline\noindent
\cite{cimiano_ontology_2006}
\section{Ontology Matching Procedure}
\label{sec:ontology-matching-procedure}
As described in section \ref{subsec:ontology-alignment}, the ontology matching process is the second component in a semantic-based integration process.
It describes the process of finding correspondences between entities of different ontologies of the same domain.
Alignment is the output of a matching processes and is used for the evolution of ontologies.
The matched ontologies enable the entities to interoperate.

Euzenat and Shvaiko \cite{euzenat_ontology_2013} defined the ontology alignment life cycle.
As shown in figure \ref{fig:ontology-alignment-life-cycle}, the life cycle workflow is subdivided into the following steps.
The alignments are first generated using a matching process.
Then the alignment result can go through an iterative evaluation and enhancement loop if necessary.
Finally the alignment can be used for procedures like query answering or merging.
The proposed alignment life cycle may be performed manually or automatically.

The ontology matching procedure presents one of the solutions to the semantic heterogeneity problems described in section \ref{subsec:ontology-alignment}.
The alignment produced from the matching can be used in various tasks such as ontology merging, query answering, and data translation.
Moreover, the same procedure can be applied to database schema matching.

Despite its benefits, the procedure also encounters some limitations, which are the types of mismatches.
When matching different ontologies, mismatches might occur on two different levels: language-level and ontology-level.
The Language-level mismatch, which describes the differences in the expressiveness of ontology languages.
The expressiveness is the presence of disjoints, negations, expression, unions, intersections, etc.
The ontology-level mismatch, which describes the differences in the structure or semantic of ontologies.

Euzenat and Shvaiko \cite{euzenat_ontology_2013} reviewed about 100 ontology matching systems.
The following can be observed: 50\% of the systems are schema-based, 25\% are mixed, i.e., rely on the schema and instances.
Moreover, most of the systems operate on two input ontologies from the same domain and handle tree-like structures by focusing on the discovery of one-to-one correspondences by computing similarity measures in the $[0\ 1]$ range.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/ontology-alignment-life-cycle}}
	\caption{Ontology alignment life cycle, according to Euzenat and Shvaiko \cite[p.\ 57]{euzenat_ontology_2013}}
	\label{fig:ontology-alignment-life-cycle}
\end{figure}
\section{Ontology Merging Procedure}
\label{sec:ontology-merging-procedure}
As described in \ref{subsec:ontology-merging}, the ontology merging process is the last component of a semantic-based integration process.
It describes the process of obtaining new ontology from two matched ontologies based on their alignment.
However, a total alignment is not required as entities with no correspondences will remain unchanged in the merged ontology.
The process is used for different purposes, such as connecting ontologies to a common upper-level ontology.

There are two types of merging systems.
The first uses the alignment result as bridge rules for the entities and creates the merged ontology accordingly.
The other system creates an ontology mapper in query forms in order to answer the queries from the merged global ontology.
\newline\noindent
\cite{euzenat_ontology_2013}

\section{The Ontology ToolChain}
\label{sec:the-ontology-toolchain}
One of the main goals of this study is to define a domain knowledge development life cycle and a toolchain to help by the creation and development of semantic domain representation.
The life cycle is used in the evolution of semantic representations, e.g., ontologies, for domains of interest.
Thus, a traditional evolution of an ontology would include its creation and potential enhancement through the merging process with other ontologies of the same domain.
As mentioned in section \ref{sec:ontology-merging-procedure}, the merging procedure requires a partial or entire alignment between entities of different ontologies of the same domain.
Therefore, the evolution of an ontology would also include a matching process to find correspondences with other ontologies of the same domain.
The last process in the life cycle is the transformation.
As mentioned in section \ref{subsec:ontology-alignment}, the most efficient matching techniques are the constraint- and instance-based, which take into consideration the structure and instances of the entities in the ontologies which need to be matched.
Thus, the data source transformation process is essential to append data structure properties and instances from the data source to its semantic representation (ontology).

As shown in figure \ref{fig:the-toolchain-structure}, the toolchain consists of four tools: composition, transformation, matching, and merging.
Each tool is used to achieve one functionality of the toolchain mentioned above and can be executed independently.
The toolchain delivers an integrated domain knowledge, which is the alignment result of the transformed knowledge from the data sources of some domain of interest.
One essential characteristic of the toolchain is the presence of the feedback mechanism.
In other words, the user is able to confirm or adjust the output at every stage.
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/the-toolchain-structure}}
	\caption{The ontology toolchain structure}
	\label{fig:the-toolchain-structure}
\end{figure}

\subsection{Composition Tool}
\label{subsec:composition-tool}
The composition tool is responsible for the creation of an ontology for a specific domain of interest.
Using the composition tool, a user can create a domain knowledge base for a particular application.
The tool is capable of applying the ontology composition procedure presented in section \ref{sec:ontology-composition-procedure}.
Figure \ref{fig:the-composition-tool-structure} depict the structure of the tool.
Some of the popular ontologies construction tools in the market are \textsf{Protégé}, \textsf{WebOnto}, \textsf{OilEd}, \textsf{SWOOP}, \textsf{OntoSaurus}, \textsf{Ontolingua Server}, \textsf{WebODE}, and \textsf{OntoEdit}.
\textsf{Protégé} and \textsf{SWOOP} were selected for the evaluation conducted in chapter \ref{cha:evaluation}.

Protégé is a popular, well-known open-source ontology construction tool developed at Stanford University.
It provides a \ac{gui} for the creation, visualization, and adjustment of ontologies, and supports different ontology language formats such as \ac{rdf}/\ac{xml}, \ac{owl}/\ac{xml}.

\ac{swoop} is a simple \ac{owl} browser and editor for ontologies developed at the University of Maryland.
\newline\noindent
\cite{abburu_survey_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/the-composition-tool-structure}}
	\caption{The composition tool structure}
	\label{fig:the-composition-tool-structure}
\end{figure}
\subsection{Transformation Tool}
\label{subsec:transformation-tool}
The transformation tool is responsible for adding the structure properties and data instances to an ontology.
Using the transformation tool, a user is able to construct an ontology that semantically describes a data source.
The user is also able to adjust the generated ontology to apply axioms and semantic relations that are not expressed in the schema of the data source.
The tool is capable of applying the data source transformation procedure presented in section \ref{sec:data-source-transformation-procedure}.
Figure \ref{fig:the-transformation-tool-structure} depict the structure of the tool.
\textsf{Protégé} and \textsf{Tarql} were selected for the evaluation conducted in chapter \ref{cha:evaluation}.

Protégé contains a built-in functionality for generating instances, annotations, and axioms from spreadsheet documents (e.g., \textsf{.xlsx} and \textsf{.xls}).

Tarql is a commant line interface, used for converting \ac{csv} files to \ac{rdfs} using \ac{sparql} syntax.
\ac{sparql} is a \ac{sql}-like query language for querying \ac{rdf}-based documents.
It is an accepted standard, however, not intended for ontology representation, but rather for querying.
It can be used to extract information from \ac{rdf} documents, and construct new ones by matching filter patterns against the target graph of the query.
\newline\noindent
\cite{paulheim_ontology-based_2011, gasevic_model_2009}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/the-transformation-tool-structure}}
	\caption{The transformation tool structure}
	\label{fig:the-transformation-tool-structure}
\end{figure}
\subsection{Matching Tool}
\label{subsec:matching-tool}
The matching tool is responsible for finding alignment between entities of the constructed ontologies: the ontology representing the domain and the ontology generated from the data source.
Using the matching tool, a user is able to find elements of two ontologies, which refer to the same concept.
The user is also able to confirm or adjust the resulted alignment in case of wrong correspondences.
The tool is capable of applying the ontology matching procedure presented in section \ref{sec:ontology-matching-procedure}.
Figure \ref{fig:the-matching-tool-structure} depict the structure of the tool.
\textsf{COMA++} and \textsf{AML} were selected for the evaluation conducted in chapter \ref{cha:evaluation}.

\ac{coma} is a schema matching tool contains six elementary, and five hybrid matching algorithms that implement string-based matching techniques such as $n$-gram and edit distance.
The ontologies are imported and encoded as a \ac{dag}, where the elements define the paths in the graph. 
\ac{coma}++ is an enhanced version of \ac{coma} that provides fragment-based matching and a \acf{gui}.

\ac{aml} is a simple version of the \ac{am} software, which offers a wide range of matching algorithms and a \acl{gui}.
The software is designed to handle large-scale ontologies and is capable of finding 1:1, 1:$m$, $n$:1, $n$:$m$ alignments between the entities.
The matching process is subdivided into two modules: similarity computation and alignment selection.
The software offers an interface to adjust the evaluation strategies and, therefore, the matching algorithm.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/the-matching-tool-structure}}
	\caption{The matching tool structure}
	\label{fig:the-matching-tool-structure}
\end{figure}
\subsection{Merging Tool}
\label{subsec:merging-tool}
The merging tool is responsible for merging a set of alignment results with the constructed domain ontology.
Using the merging tool, a user is able to extend the constructed domain ontology with the interconnections found within the semantic representation of the data sources.
The tool is capable of applying the ontology merging procedure presented in section \ref{sec:ontology-merging-procedure}.
Figure \ref{fig:the-merging-tool-structure} depict the structure of the tool.
\textsf{Protégé} and \textsf{OntoMerge} were selected for the evaluation conducted in chapter \ref{cha:evaluation}.

Protégé contains a built-in functionality for merging ontologies, whether by extending an existing ontology or creating a new one.
It is also capable of merging alignment results encoded in an ontology language format such as \ac{rdf}/\ac{xml}, \ac{owl}/\ac{xml}.

OntoMerge is a system for ontology merging and translation.
The merging procedure is conducted by taking the union of the axioms defined in both ontologies.
Bridging axioms and rules are provided by a domain expert or matching algorithm to relate the terms in both ontologies.
The translation task follows the merging procedure, which generates ontology extensions and query answering for both ontologies.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/approach/the-merging-tool-structure}}
	\caption{The merging tool structure}
	\label{fig:the-merging-tool-structure}
\end{figure}
\section{The ToolChain Criteria}
\label{sec:the-toolchain-criteria}
The next step in the approach is to evaluate the proposed life cycle and tools.
For this task, a set of mandatory criteria is assigned in order to be investigated and evaluated.
Those criteria are: \textsf{time-consuming}, \textsf{quality}, \textsf{validation}, and \textsf{post-processing}.
The reason behind choosing those criteria lies in their interrelation.
In other words, the score assigned to one criterion might have an impact on the others.
Moreover, investigating those criteria will help in answering the research questions in section \ref{sec:goal-and-research-question}.
Further criteria can also be investigated in future research works.
\subsection{Time-Consuming}
\label{subsec:time-consuming}
The time-consuming criteria aim to evaluate the time spent during the interaction with the tool to get the first output regardless of the reiteration process, which is responsible for optimize the output.
The post-processing criterion attends to evaluate the reiteration process.
This criterion is rated by one of the following values: \textsf{low}, \textsf{medium}, and \textsf{high}.
\textsf{Low} is the optimal value, which indicates that a user with limited expertise is able to interact with the tool.
In other words, the tool is easily manageable and does not require any substantial experience.
\subsection{Quality}
\label{subsec:quality}
The quality criteria aim to evaluate the anomaly between the actual and expected output from the tool regardless of the reasoning process, which is responsible for checking conceptual mistakes.
This criterion is rated by one of the following values: \textsf{low}, \textsf{medium}, and \textsf{high}.
\textsf{High} is the optimal value, which indicates that the output satisfies the user's expectation.
\subsection{Validation}
\label{subsec:validation}
The quality criteria aim to evaluate the correctness of the output, also known as reasoning.
Reasoning on ontologies is used to discover conceptual mistakes.
This criterion is rated by one of the following values: \textsf{low}, \textsf{medium}, and \textsf{high}.
\textsf{High} is the optimal value, which indicates that the output is semantically correct.
\newline\noindent
\cite{paulheim_ontology-based_2011}
\subsection{Post-Processing}
\label{subsec:post-processing}
The quality criteria aim to evaluate the complexity of the reiteration process to optimize the output if needed.
This criterion is rated by one of the following values: \textsf{low}, \textsf{medium}, and \textsf{high}.
\textsf{Low} is the optimal values, which indicates that no reiteration is needed, and no conceptual mistakes are found in the result.

\section{Evaluation Matrix}
\label{sec:evaluation-matrix}
The evaluation matrix approach is used for the evaluation of proposed tools against the specified criteria.
Table \ref{tab:evaluation-matrix-example} represents an example for the evaluation matrix.
Since one of the objectives of this thesis is to investigate the gap between domain experts and ontology engineers, both roles are evaluated separately.
The evaluation's results are going to help in identifying the advantages and limitations of the domain engineering approach.
Moreover, the deduced results are also going to help to answer the research question in section \ref{sec:goal-and-research-question}.

Based on the conducted literature and related work review in chapter \ref{cha:background} and \ref{cha:related-work}, the following assumptions are expected.
A sub-optimal score of one criterion will propagate and affect the score of at least one other criterion.
Furthermore, based on the evaluated user's role, some criteria will have an optimal score as a result of the user's expertise.
\begin{table}[h]
	\centering
	\begin{tabular}{r c c}
		& Tool A & Tool B\\
		\hline
		Time-Consuming & &\\
		Quality & &\\
		Validation & &\\
		Post-Processing & &\\
	\end{tabular}
	\caption{Evaluation matrix example}
	\label{tab:evaluation-matrix-example}
\end{table}






%===================================================
%2 roles:
%consider ontology engineer and domain expert.
%ontology engineering is a context in which users are confronted with heterogeneous ontologies, and, more generally, the task of designing, implementing, and maintaining ontology-based applications \cite[p.\ 3]{euzenat_ontology_2013}.
%this activity requires ontology matching support because ontology engineering has to deal with multiple, distributed and evolving ontologies \cite[p.\ 3]{euzenat_ontology_2013}.
%examine if both knowledge is needed to obtain optimal result.
%
%tools:
%2 alternatives for every tool in the tool chain
%composition; \textit{Protégé} and \textit{\ac{swoop}}
%transformation: \textit{Protégé} and \textit{TARQL}
%matching: \textit{\ac{aml}} and \textit{\ac{coma}}
%meging: \textit{Protégé} and \textit{OntoMerge}
%
%criteria:
%time-consuming, quality, validation, and post-processing
%
%8 evaluation matrices:
%8 evaluation matrices for each role and tool.
%
%table description:
%table \ref{tab:evaluation-matrix-example} presents an example for an evaluation matrix
%
%expectation:
%low scores affects when occurring in early stages of the tool chain.
%non-overlapping low scores in both decision matrices.
%a role with ontology engineering and domain expertise would enhance the scores.
%===================================================
%\section{Processes in Data Mining Layer}
%\label{sec:processes-in-data-mining-layer}

%\section{Use Case Diagram}
%\label{sec:use-case-diagram}
%aim/goal
%procedure
%system's actors
%figure description
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/use-case}}
%	\caption{Use Case Diagram}
%	\label{fig:use-case}
%\end{figure}
%
%\subsection{Clustering-based Approach}
%\label{subsec:clustering-based-approach}
%
%\subsection{Indexing-based Approach}
%\label{subsec:indexing-based-approach}

%\ac{ola} is an ontology matching system designed with the idea of balancing the contribution of each of the components that compose an ontology, e.g., classes, constraints, and data instances \cite[p.\ 247]{euzenat_ontology_2013}
%first compiles the input ontologies into graph structures, unveiling all relationships between entities \cite[p.\ 247]{euzenat_ontology_2013}
%the similarity between nodes of the graphs depends on the category of node considered, e.g. class, property, and it takes into account all the features of this category, e.g., superclasses and properties \cite[p.\ 247]{euzenat_ontology_2013}
%the distance between nodes is expressed as a system of equations based on string-based, language-based and structure-based similarities \cite[p.\ 247]{euzenat_ontology_2013}
%the algorithm starts with base distance measures computed from labels and concrete data types then iterates until no improvement is produced \cite[p.\ 247]{euzenat_ontology_2013}

%\section{System Architecture}
%\label{sec:system-architecture}
%structure:
%as shown in figure \ref{fig:layered-architecture}, the system is divided into four layers; configuration, learning, matching, and merging.
%layer name describes its functionality.
%each layer consist of at least one services.
%additional services can be plugged to prove the layer's overall performance, or to be precise, expected output.
%
%layers:
%the four layers are presented in figure \ref{fig:layered-architecture}
%each layer is triggered independently.
%some layers depends on the output of their previous ones, therefore a top down execution sequence is essential.
%
%role:
%the system delivers an integrated domain knowledge base.
%this domain knowledge is aligned with the extracted knowledge bases from the heterogenous data sources.
%
%procedure:
%using the configuration layer, a user or domain expert creates a domain knowledge base for a particular application.
%the learning layer extract a knowledge base from data sources that fit the application's domain.
%the data sources are in correlation with the application domain.
%the matching layer uses different parallel composition matchers to build an alignment between the extracted knwoledge base from a data source and the created domain knowledge.
%the merging layer merges all alignment results an build an extended domain knowledge base, covering all knowledges presented in data sources which needs to be integrated.
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/layered-architecture}}
%	\caption{Layered Architecture}
%	\label{fig:layered-architecture}
%\end{figure}
%
%\subsection{Configuration Layer}
%\label{subsec:configuration-layer}
%components:
%as shown in figure \ref{fig:configuration-layer}, the configuration layer provides a configuration service.
%it is responsible for generating a domain knowledge through an interaction with a domain expert.
%it is essential to point out the need of constructing a well-defined ontology with the help of the components described in \ref{sec:ontology-composition-procedure}.
%constructing such informative ontology enhance the performance of the matching layer.
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/configuration-layer}}
%	\caption{Configuration Layer}
%	\label{fig:configuration-layer}
%\end{figure}
%
%\subsection{Learning Layer}
%\label{subsec:learning-layer}
%components:
%as shown in figure \ref{fig:learning-layer}, the learning layer provides a learning service.
%it takes as input a data source which consists of a schema description and data sets, and generates a knowledge base.
%if required, a domain expert can adjust the generated knowledge.
%in the context of this work, the learning service is not based on an artificial intelligence learning procedure.
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/learning-layer}}
%	\caption{Learning Layer}
%	\label{fig:learning-layer}
%\end{figure}
%
%\subsection{Matching Layer}
%\label{subsec:matching-layer}
%components:
%as shown in figure \ref{fig:matching-layer}, the matching layer provides a matching service.
%it takes as input the generated domain knowledge and data source kowledge from the configuration and learning layers respectively.
%it matches the inputs and generate an alignment.
%if required, a domain expert can adjust the generated alignment result.
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/matching-layer}}
%	\caption{Matching Layer}
%	\label{fig:matching-layer}
%\end{figure}
%
%\subsection{Merging Layer}
%\label{subsec:merging-layer}
%components:
%as shown in figure \ref{fig:merging-layer}, the merging layer provides a merging service.
%it takes as input the generated domain knowledge and alignment result from the the configuration and matching layers respectively.
%it extends the domain knowledge by integrating the alignment.
%if required, a domain expert can adjust the domain knowledge.
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics[width=\textwidth]{./figure/approach/merging-layer}}
%	\caption{Merging Layer}
%	\label{fig:merging-layer}
%\end{figure}

%selection:
%learning by googling by gathering knowledge from data source.
%manual filtering with help of domain expert by specifying context of interest.
%\ac{pankow} is a pattern generation mechanism which creates pattern strings out of a certain pattern schema conveying a specific semantic relation \cite[p.\ 250]{cimiano_ontology_2006}.
%it counts the occurences of these pattern strings on the web using the Google{\textit{\texttrademark}} API \cite[p.\ 250]{cimiano_ontology_2006}.
%the approach is semi-supervised \cite[p.\ 250]{cimiano_ontology_2006}.

%\begin{table}[h]
%	\centering
%	\begin{tabular}{l c c c c c c}
%		& time-consuming & quality & validation & post-processing\\
%		\hline
%		composition &
%		&
%		&
%		&
%		\\
%		transformation &
%		&
%		&
%		&
%		\\
%		matching &
%		&
%		&
%		&
%		\\
%		merging &
%		&
%		&
%		&
%		\\
%	\end{tabular}
%	\caption{Evaluation matrix for the ontology tool chain criteria}
%	\label{tab:decision-matrix-for-the-ontology-tool-chain-criteria}
%\end{table}

%since \ac{rdf} is used for defining \ac{owl}, i.e., each valid \ac{owl} document is also a valid \ac{rdf} document \cite[p.\ 30]{paulheim_ontology-based_2011}.
%\ac{owl} document can be serialized in all syntaxes that exist for \ac{rdf}, i.e., \ac{rdf}-\ac{xml} or N3 \cite[p.\ 30]{paulheim_ontology-based_2011}.
%further possible syntaxes are \ac{owl} functional-style syntax and Manchester syntax \cite[p.\ 31]{paulheim_ontology-based_2011}.
%alternative to protégé is to construct the \ac{owl} documents with one of the proposed syntax by hand.