% !TeX spellcheck = en_US
\chapter{Background}
\label{cha:background}
Over the past years, reusability has always become a challenge.
It evolved from considering a small chunk of source lines of code, to a complete process.
The greatest challenge today lies in the reusability of an entire domain of interest.
This chapter introduced two well-known research approaches: syntactic- and semantic-based integration.
As mentioned before, the main research interest aims to investigate domain development on semantic bases.
A critical component in the semantic representation of a domain is the adaptation to changes.
Therefore, this chapter reviews the integration concept to help understand the challenges and limitations in each approach.

\section{Syntactic-based Integration}
\label{sec:syntactic-based-integration}
The syntactic-based integration is the process of finding similarity by the interpretation of the input regarding its structure.
Using syntactic matching such as string-based matching algorithms, a similarity measure can be calculated to decide whether two entities share a similar structure and therefore meaning.
In some cases, a syntactic-based similarity measures can be impractical and unreliable.
For example, \textsf{Blu-ray disc}, \textsf{BD}, \textsf{B.D.}, and \textsf{BD-Rom} can be considered equivalent in some contexts.
However, in some other context, \textsf{BD} may mean \textsf{Birthday} and in some other \textsf{Business development}.
Levenshtein (1965) introduced \textit{Edit Distance}, a widely-used similarity measure to compare two string.
The algorithm determines whether two strings are alike by calculating how many atomic actions are required to transform one string into the other one.
The atomic actions are addition, deletion, and replacement of string's characters.
Luckily, in most cases, syntactic variations of the same entity often share nearby spellings and abbreviations.
\newline\noindent
\cite{euzenat_ontology_2013, ehrig_ontology_2007}

\subsection{String Matching}
\label{subsec:string-matching}
The string matching procedure aims to find strings that refer to the same entities.
The procedure is used in data integration tasks, including schema and data matching, and information extraction.
The procedure computes a similarity score between two given strings.
The matched strings can be merged or linked to improve data quality and provide more comprehensive information.
For example, the strings \textsf{\small{Alan Mathison Turing}} and \textsf{\small{Alan M. Turing}} may refer to the same person.
The procedure has two challenges: accuracy and scalability.
The computed similarity score would deliver an unsatisfying result in cases like typing and \ac{ocr} errors, or custom abbreviation.
Moreover, data sources are unlikely to contain semantic information to determine whether two strings refer to the same entity.
\newline\noindent
\cite{doan_principles_2012, dama_the_2010}
\subsubsection{Similarity Measures Matching Techniques}
\label{subsubsec:similarity-measures-matching-techniques}
Similarity measures operate by mapping a pair of strings $(x,y)$ into a number in the range $[0,1]$.
The higher the score, the more likely that $x$ and $y$ matches.
This can be represented by the following equation: $s(x,y) \geq t$, where $t$ is a predefined threshold.
A threshold is a value at or above which the pair of strings are considered to be similar.
Table \ref{tab:an-example-of-matching-person-names-according-to-doan-et-al} presents an example of matching person names using similarity measures techniques.
Applying similarity measure to all pairs of strings in two data sets $X$ and $Y$ is very costly $O(|X||Y|)$ and therefore, impractical.
This scalability problem can be overcome using a solution called \textit{blocking solution}.
The procedure calls a method \textsf{FindCands} that finds all strings in a data set $Y$ that may match a given string $x$.
It takes $O(|X||Z|)$, where $Z$ is the so-called \textit{umbrella set} of $x$, obtained from the \textsf{FindCands} method.
\textsf{FindCands} uses techniques based on indexing or filtering heuristics to find all potential strings to match $x$.
\newline\noindent
\cite{doan_principles_2012}
\begin{table}[h]
	\centering
	\begin{tabular}{c c c}
		Set $X$ & Set $Y$ & Matches\\
		\hline
		$x_1$ = Dave Smith & $y_1$ = David D. Smith & $(x_1,y_1)$\\
		$x_2$ = Joe Wilson & $y_2$ = Daniel W. Smith & $(x_3,y_2)$\\
		$x_3$ = Dan Smith\\
		(a) & (b) & (c)
	\end{tabular}
	\caption{An example of matching person names, according to Doan et al.\ \cite[p.\ 96]{doan_principles_2012}}
	\label{tab:an-example-of-matching-person-names-according-to-doan-et-al}
\end{table}
\subsubsection{Sequence-based Similarity Measures Techniques}
\label{subsubsec:sequence-based-similarity-measures-techniques}
The sequence-based similarity measures treat the strings as sequences of characters.
It computes the cost of transforming one string to the other.
The smaller the cost, the more likely the two strings match.
\textit{The Needleman-Wunch Measure} and \textit{The Smith-Waterman Measure} are two very famous sequence-based similarity measures algorithms.
\newline\noindent
\cite{doan_principles_2012}
\subsubsection{\normalfont {\textit{The Needleman-Wunch Measure}}}
\label{subsubsec:the-needleman-wunch-measure}
The Needleman-Wunch measure belongs to the sequence-based similarity measures.
It generalizes the Levenshtein distance described in section \ref{sec:syntactic-based-integration}.
The arithmetic formula is shown in equation \ref{equ:the-needleman-wunch-measure-equation}.
The measure calculates an alignment between two strings based on a set of correspondences between their characters.
The score of the alignment is the sum of the scores of all correspondences in the alignment, minus the penalties for gaps.
\newline\noindent
\cite{doan_principles_2012}
\begin{equation}
	\begin{aligned}
		s(i,j)&=max
		\begin{cases}
			s(i-1,j-1)+c(x_i,y_j)\\
			s(i-1,j)-c_g\\
			s(i,j-1)-c_g\\
		\end{cases}\\
		s(0,j)&=-jc_g\\
		s(i,0)&=-ic_g\\
	\end{aligned}
	\label{equ:the-needleman-wunch-measure-equation}
\end{equation}
\subsubsection{\normalfont {\textit{The Smith-Waterman Measure}}}
\label{subsubsec:the-smith-waterman-measure}
The Smith-Waterman measure also belongs to the sequence-based similarity measures.
Different to the Needleman-Wunch, this measure finds two substrings of $x$ and $y$ that are most likely to be similar, then return the score computed as the score for $x$ and $y$.
The matching computation can start at any position in the strings, which helps to ignore prefixes or suffixes.
The arithmetic formula is shown in equation \ref{equ:the-smith-waterman-measure-equation}.
Consider the two strings \textsf{(Prof.\ Max S.\ Mustermann, TU Clausthal)} and \textsf{(Max S. Mustermann, Professor)}.
The measure computes a local alignment matching by ignoring particular prefixes, e.g., Prof., and suffixes, e.g., TU Clausthal.
\newline\noindent
\cite{doan_principles_2012}
\begin{equation}
	\begin{aligned}
		s(i,j)&=max
		\begin{cases}
			0\\
			s(i-1,j-1)+c(x_i,y_j)\\
			s(i-1,j)-c_g\\
			s(i,j-1)-c_g\\
		\end{cases}\\
		s(0,j)&=0\\
		s(i,0)&=0\\
	\end{aligned}
	\label{equ:the-smith-waterman-measure-equation}
\end{equation}
\subsubsection{Set-based Similarity Measures Techniques}
\label{subsubsec:set-based-similarity-measures-techniques}
The set-based similarity measures treat the string as sets of generated tokens.
To generate tokens from strings, the space character and common stop words, (e.g., the, and, of), are excluded and used as delimiters.
Another common type of tokens is \textit{q-grams}, which divides the string into substrings of length $q$.
For example, the set of all 3-grams of \textsf{max mustermann} is \{\textsf{\#\#m}, \textsf{\#ma}, \textsf{max}, \dots, \textsf{ann}, \textsf{nn\#}, \textsf{n\#\#}\}.
\newline\noindent
\cite{doan_principles_2012}
\subsubsection{\normalfont {\textit{The TF/IDF Measure}}}
\label{subsubsec:the-tf-idf-measure}
The \acl{tf}/\acl{idf} (\acs{tf}/\acs{idf}) is a set-based similarity measure.
It considers two strings to be similar if they share distinguishing terms and is mostly used to find documents that are relevant to search keywords.
The arithmetic formula is shown in equation \ref{equ:the-tf-idf-measure-equation}.
For example, to be considered are the three strings $x$ = (\textsf{Apple Corporation, CA}), $y$ = (\textsf{IBM Corporation, CA}), and $z$ = (\textsf{Apple Corp}).
A traditional sequence-based measures would match $x$ with $y$ as $s(x,y)$ higher than $s(x,z)$.
However, the \acs{tf}/\acs{idf} measure is able to recognize \textsf{Apple} as a distinguishing term, and thus would correctly match $x$ and $z$ with a higher score.
The measure converts each string to a bag of terms using \ac{ir} terminology.
For example, $x$ = \textsf{aab} is converted into document $B_{x}$ = \{\textsf{a}, \textsf{a}, \textsf{b}\}.
Then \ac{tf} and \ac{idf} are computed: $tf(t,d)$ refers to the number of times $t$ occurs in $d$, and $idf(t)$ refers to the total number of documents in the collection divided by the number of documents that contain $t$.
The \ac{tf}/\ac{idf} score between $p$ and $q$ can be computed as the cosine of the angle between these two vectors.
\newline\noindent
\cite{doan_principles_2012}
\begin{equation}
	\begin{aligned}
		s(p,q)&=\dfrac{\sum_{t\in T}{v_{p}(t)\cdot v_{q}(t)}}{\sqrt{\sum_{t\in T}{v_{p}(t)}^{2}}\cdot \sqrt{\sum_{t\in T}{v_{q}(t)}^{2}}}
	\end{aligned}
	\label{equ:the-tf-idf-measure-equation}
\end{equation}
\subsubsection{Hybrid Similarity Measures Techniques}
\label{subsubsec:hybrid-similarity-measures-techniques}
The hybrid similarity measures combine the advantages of the sequence-based and set-based measures.
While Sequence-based techniques cover matching of misspelled or abbreviated strings, the set-based techniques cover matching strings based on shared distinguishing terms.
\newline\noindent
\cite{doan_principles_2012}
\subsubsection{\normalfont {\textit{The Soft \ac{tf}/\ac{idf} Measure}}}
\label{subsubsec:the-soft-tf-idf-measure}
The soft \ac{tf}/\ac{idf} is a hybrid similarity measure.
It enhances the \ac{tf}/\ac{idf} measure \ref{subsubsec:the-tf-idf-measure} by softening the exact match requirement and requiring similar matching instead.
The arithmetic formula is shown in equation \ref{equ:the-soft-tf-idf-measure-equation}.
For example, to be considered are the tree strings $x$ = (\textsf{Apple Corporation, CA}), $y$ = (\textsf{IBM Corporation, CA}), and $z$ = (\textsf{Aple Corp}).
The traditional \ac{tf}/\ac{idf} measures would not match $x$ with $z$ because the term \textsf{Apple} in $x$ does not match the misspelled term \textsf{Aple} in $z$.
The Soft \ac{tf}/\ac{idf} first generate two documents $B_{x}$ and $B_{y}$ which contain terms close to each other, then computes the $s(x,y)$ as in the traditional \ac{tf}/\ac{idf} measure.
The measure creates two documents $B_{x}$ and $B_{y}$ as described by the \ac{tf}/\ac{idf} measure.
Then it computes $close(x,y,k)$ as a set of all terms in $B_{x}$ that have a close term in $B_{y}$ that satisfies $s'(t,u) \geq k$, where $k$ is a predefined threshold.
$s(x,y)$ as in the traditional \ac{tf}/\ac{idf} score are computed.
$v_{x}$ and $v_{y}$ are the generated feature vectors and $u_{*}$ is the term that maximizes $s'(t,u)$ for all $u\in B_{y}$.
\newline\noindent
\cite{doan_principles_2012}
\begin{equation}
	\begin{aligned}
		s(x,y)&=\sum_{t\in close(x,y,k)}{v_{x}(t)\cdot v_{y}(u_{*})\cdot s'(t,u_{*})}
	\end{aligned}
	\label{equ:the-soft-tf-idf-measure-equation}
\end{equation}
\subsection{Data Matching}
\label{subsec:data-matching}
Similar to the string matching procedure, data matching aims to find structured data items that describe the same entities.
Similar data can be joined from sources that have a different schema.
For example, the tuples \textsf{(Alan Mathison Turing, 555-234-5678, Maida Vale)} and \textsf{(Alan M. Turing, 234-5678, Maida Vale)} refer to the same person.
Also similar to the string matching procedure, accuracy and scalability are its main challenges.
Clustering and collective matching techniques aim to address the accuracy challenge.
\newline\noindent
\cite{doan_principles_2012}
\subsubsection{Clustering Matching Techniques}
\label{subsubsec:clustering-matching-techniques}
\subsubsection{\normalfont {\textit{\ac{ahc}}}}
\label{subsubsec:agglomerative-hierarchical-clustering}
The \ac{ahc} is a clustering matching technique that divides data into a set of groups called clusters where members of one cluster belong together.
For example, given a set of tuples $A$, \ac{ahc} partitions $A$ into a set of clusters, such that all tuples in each cluster refer to an entity, tuples in different clusters refer to different entities.
The algorithm begins by putting each tuple in $A$ into a single cluster.
Then iteratively merges the two most similar clusters until the similarity score drops below a predefined threshold.
The similarity can be computed using different methods: single link, complete link, and average link.
The single link compute the similarity score as the minimal score between all tuple pairs $s(c,d)=min_{x_{i}\in c,y_{i}\in d} sim(x_{i},y_{j})$.
The complete link maximize the computed score between all pairs of tuples $s(c,d)=max_{x_{i}\in c,y_{i}\in d} sim(x_{i},y_{j})$.
The average link considers the total number of pairs $s(c,d)=\sum_{x_{i}\in c,y_{i}\in d} sim(x_{i},y_{j})/n$.
\newline\noindent
\cite{doan_principles_2012}
\subsubsection{Collective Matching Techniques}
\label{subsubsec:collective-matching-techniques}
\subsubsection{\normalfont {\textit{Advanced Agglomerative Hierarchical Clustering}}}
\label{subsubsec:advanced-agglomerative-hierarchical-clustering}
The advanced \ac{ahc} enhances the \ac{ahc} measuring procedure by taking into consideration the correlation among all tuples.
The arithmetic formula is shown in equation \ref{equ:the-advanced-agglomerative-hierarchical-clustering-equation}.
$sim_{attributes}(A,B)$ computes a similarity score between $A$ and $B$ based only their attributes, while $sim_{neighbors}(A,B)$ computes a similarity score between the neighborhood of $A$ and $B$.
Then, the most similar clusters are merged.
\newline\noindent
\cite{doan_principles_2012}
\begin{equation}
	\begin{aligned}
		sim(A,B)&=\alpha\cdot sim_{attributes}(A,B)+(1-\alpha)\cdot sim_{neighbors}(A,B)
	\end{aligned}
	\label{equ:the-advanced-agglomerative-hierarchical-clustering-equation}
\end{equation}
\subsection{Schema Matching and Mapping}
\label{subsec:schema-matching-and-mapping}
The idea behind integration is giving the user a way to interact with the data through a single schema, also known as mediation schema.
The mediated schema is a logical schema that contains the relevant information of the domain.
Schema matching specifies how the elements of the source schema correspond to the mediated schema.
It also indicates how to translate data across their sources and the mediated schema.
For example, attribute \textsf{\small{detail}} in one source corresponds to attribute \textsf{\small{information}} in another, and \textsf{\small{name}} in one source is a concatenation of \textsf{\small{firstname}}, and \textsf{\small{lastname}} in another.
This approach requires an understanding of the semantics of the source and mediated schemas in order to construct the mapping between them.
\newline\noindent
\cite{doan_principles_2012}
\section{Semantic-based Integration}
\label{sec:semantic-based-integration}
The semantic-based integration is the process of finding the similarity between two concepts by measuring the overlap of their instance sets.
The procedure is used to discover the possible similarity between a new instance and the set of instances already presented.
One of its challenges lies in the fact that a schema does not adequately describe its meaning, which makes it hard for a program to process it.
Furthermore, two schema elements may share the same name and yet refer to different real-world concepts.
Conversely, two attributes with different names can refer to the same real-world concept.
In some cases, it is hard to elaborate the matches into mapping.
For example \textsf{Event.localTime} $\approx$ \textsf{Event.universalTime} $+$ \textsf{Location.timeZone}.
\newline\noindent
\cite{euzenat_ontology_2013, doan_principles_2012, paliuras_knowledge-driven_2011}
\subsection{Ontology Learning}
\label{subsec:ontology-learning}
In computer science, an ontology is an engineering artifact used to describe a real-world concept.
It also consists of a vocabulary and axioms to describe their meaning.
It is used to represent a conceptual model for a specific domain.
The symbolic representation of knowledge can be separated from the aspects related to the application and therefore reused across different systems.
This representation makes it possible for a computer to process it.
Figure \ref{fig:ontology-example-according-to-cimiano} shows an ontology example with a set of entities and the relation between them.
\newline\noindent
\cite{cimiano_ontology_2006, maedche_ontology_2002}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/literature-review/ontology-example}}
	\caption{Ontology example, according to Cimiano \cite[p.\ 11]{cimiano_ontology_2006}}
	\label{fig:ontology-example-according-to-cimiano}
\end{figure}
\subsection{Ontology Alignment}
\label{subsec:ontology-alignment}
The ontology alignment is the result of a matching process.
It is used to identify relations between individuals of multiple ontologies.
The identified correspondences are among others, one of the following: equivalence, consequence, and disjointness.
This procedure is the first step toward the ontology merging and query answering procedures.
Figure \ref{fig:ontology-alignment-according-to-euzenat-and-shvaiko} shows an alignment example between two ontologies of the same domain.
There are two classes of ontology matching techniques: element-level and structure-level.
The element-level concludes string-based, language-based, and constrain-based techniques.
The structure-level concludes graph-based, taxonomy-based, model-based, and instance-based techniques.
The main challenge presented in this procedure is involving the user to validate and adjust the matching process without being lost in the design choices.
Another challenge is the types of heterogeneity that might be presented in the ontologies.
Syntactic heterogeneity is the case where two ontologies are not described with the same ontology language.
The terminological heterogeneity which means variations in the names when referring to the same entities.
Conceptual heterogeneity is the case when modeling the same domain of interest but in different ways.
Semiotic heterogeneity is the case when entities are interpreted differently.
\newline\noindent
\cite{euzenat_ontology_2013, ehrig_ontology_2007}
\begin{figure}[h]
	\centering
	\frame{\includegraphics[width=\textwidth]{./figure/literature-review/ontology-alignment}}
	\caption{Ontology alignment, according to Euzenat and Shvaiko \cite[p.\ 45]{euzenat_ontology_2013}}
	\label{fig:ontology-alignment-according-to-euzenat-and-shvaiko}
\end{figure}
\subsubsection{String-based Matching Techniques}
\label{subsubsec:string-based-matching-techniques}
The string-based procedure belongs to the element-level matching techniques.
It considers a string as a sequence of letters.
The more similar the strings, the more likely they refer to the same real-world concept.
The procedure is used to match names and descriptions of ontology's entities.
Some examples of the string-based techniques are edit distances, and $n$-gram similarity measures.
\newline\noindent
\cite{euzenat_ontology_2013}
\subsubsection{\normalfont {\textit{Token-based Distances}}}
\label{subsubsec:token-based-distances}
The Token-based Distances belongs to the string-based matching techniques.
It considers a multiset of words $s$ as a vector in which dimension is a term, and each position is the number of occurrences.
After transforming the entities into vectors, metric space distances can be used to compute the similarity such as Euclidean distance, Manhattan distance, or Minkowski distance.
For example, \textsf{InProgress} becomes \textsf{In} and \textsf{Progress}, \textsf{similarity-based measures} becomes \textsf{similarity}, \textsf{based} and \textsf{measures}.
The arithmetic formula is shown in equation \ref{equ:token-based-distances-equation}.
$\vec{s}$ and $\vec{t}$ are the vectors corresponding to two strings $s$ and $t$ in a vector space $V$ \cite[p.\ 93]{euzenat_ontology_2013}.
the cosine similarity is the function $\sigma_{v}: V\times V \rightarrow [0\ 1]$.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{equation}
	\begin{aligned}
		\sigma_{v}(s,t)&=\frac{\sum_{i\in |V|}\vec{s_{i}}\times \vec{t_{i}}}{\sqrt{\sum_{i\in |V|}\vec{s_{i}}^{2}\times \sum_{i\in |V|}\vec{t_{i}}^{2}}}
	\end{aligned}
	\label{equ:token-based-distances-equation}
\end{equation}
\subsubsection{Constraint-based Matching Techniques}
\label{subsubsec:constraint-based-matching-techniques}
The constraint-based procedure belongs to the element-level matching techniques family.
The procedure deals with the internal constraints of the defined entities, such as types, multiplicity of attributes, and keys.
\newline\noindent
\cite{euzenat_ontology_2013}
\subsubsection{\normalfont {\textit{Internal Structure-based Techniques}}}
\label{subsubsec:internal-structure-based-techniques}
The internal structure-based techniques belong to the constraint-based matching techniques.
Its mode of operation is to calculate a similarity measure based on the structure of entities, in addition to comparing the names and identifiers.
The structure of entities considering their properties (names, keys, data types, domains, cardinalities), their relations, and their multiplicity.
The procedure is combined with element-level techniques to reduce the number of candidate correspondences and eliminate incompatible properties.
Figure \ref{fig:internal-structure-comparison-according-to-euzenat-and-shvaiko} shows and example for an internal structure comparison technique, considering the properties and cardinality associated with the \textsf{Product} and \textsf{Volume}.
The procedure's limitation is caused by the fact that the internal structure does not provide much information to compare, as many different objects can have the same properties with the same data types.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics{./figure/literature-review/internal-structure-comparison}}
	\caption{Internal structure comparison, according to Euzenat and Shvaiko \cite[p.\ 107]{euzenat_ontology_2013}}
	\label{fig:internal-structure-comparison-according-to-euzenat-and-shvaiko}
\end{figure}
\subsubsection{Graph-based Matching Techniques}
\label{subsubsec:graph-based-matching-techniques}
The graph-based procedure belongs to the structure-level matching techniques.
It considers the input ontologies as a graph whose edges are labeled by relation names.
In case two nodes are similar, their neighbors must also be similar.
The procedure finds correspondences between elements by finding a common homomorphic subgraph.
Graph homomorphisms are mappings of the vertices while preserving adjacency.
For example, given $G$ and $H$ two graphs, homomorphisms of $G$ to $H$ are paths in $G$ mapped in $H$ without increasing the distances.
The example \ref{equ:graph-based-matching-techniques-example} represents a relation-based example on the \textsf{subClasses} of \textsf{Book} from the ontology presented in figure \ref{fig:ontology-alignment-according-to-euzenat-and-shvaiko}.
\newline\noindent
\cite{euzenat_ontology_2013, hell_graphs_2004}
\begin{equation}
	\begin{aligned}
	subclass(\textsf{Book})=subclass^{-}(\textsf{Book})&=\{\textsf{Science}, \textsf{Pocket}, \textsf{Children}\}\\
	subclass^{+}(\textsf{Book})&=\{\textsf{Science}, \textsf{Pocket}, \textsf{Textbook}, \textsf{Popular}, \textsf{Children}\}\\
	subclass^{-1}(\textsf{Book})&=\{\textsf{Product}\}\\
	subclass^{\uparrow}(\textsf{Book})&=\{\textsf{Textbook}, \textsf{Popular}, \textsf{Pocket}, \textsf{Children}\}\\
	\end{aligned}
	\label{equ:graph-based-matching-techniques-example}
\end{equation}
\subsubsection{Instance-based Matching Techniques}
\label{subsubsec:instance-based-matching-techniques}
The instance-based procedure belongs to the structure-level matching techniques family.
The mode of its operation is to compare sets of instances of classes to determine whether these classes match or not.
For example, $A$ and $B$ are two classes with one of the possible relationships.
Equal $(A\cap B=A=B)$, contains $(A\cap B=A)$, contained-in $(A\cap B=B)$, disjoint $(A\cap B=\emptyset)$.
The matching is facilitated when two ontologies share the same set of instances.
However, in the case of incorrect data, the system may deliver wrong correspondences.
To overcome this limitation, Hamming distance can be used to calculate the corresponds based on the size of symmetric differences normalized by the size of the union.
The arithmetic formula is shown in equation \ref{equ:instance-based-matching-techniques-equation}.
Hamming distance can still produce a short distance even in case of misclassified instances due to the usage of symmetric normalization.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{equation}
	\begin{aligned}
		\sigma(x,y)&=\frac{|x\cup y-x\cap y|}{|x\cup y|}
	\end{aligned}
	\label{equ:instance-based-matching-techniques-equation}
\end{equation}
\subsection{Ontology Merging}
\label{subsec:ontology-merging}
The ontology merging procedure is used to integrate matched ontologies $o$ and $o^{'}$ into a new ontology $o^{''}$ based on a computed alignment.
For example, consider the following alignment results: (id = isbn) and (book ${\leq}$ Volume).
The resulting ontology will have the following individuals: (o:id owl:equivalentProperty o´:isbn) and (o:Book rdfs:subClassOf o´:Volume).
The procedure does not require a total alignment between the individuals of both ontologies.
Entities with no correspondences will remain unchanged.
Figure \ref{fig:ontology-merging-according-to-euzenat-and-shvaiko} shows the internal functionality of the ontology merging procedure.
\newline\noindent
\cite{euzenat_ontology_2013}
\begin{figure}[h]
	\centering
	\frame{\includegraphics{./figure/literature-review/ontology-merging}}
	\caption{Ontology merging, according to Euzenat and Shvaiko \cite[p.\ 379]{euzenat_ontology_2013}}
	\label{fig:ontology-merging-according-to-euzenat-and-shvaiko}
\end{figure}






%===================================================
%\subsection{Ontology Alignment}
%types of heterogeneity; \cite[p.\ 37]{euzenat_ontology_2013}.
%syntactic heterogeneity: two ontologies not expressed in the same ontology language \cite[p.\ 37]{euzenat_ontology_2013}.
%example: two ontologies are modeled by using different knowledge representation formalisms \cite[p.\ 37]{euzenat_ontology_2013}.
%
%terminological heterogeneity: variations in names when referring to the same entities in different ontologies \cite[p.\ 38]{euzenat_ontology_2013}.
%example: use of different natural languages such as paper and article \cite[p.\ 38]{euzenat_ontology_2013}.
%
%conceptual/semantic heterogeneity: differences in modeling the same domain of interest \cite[p.\ 38]{euzenat_ontology_2013}.
%example: two ontologies describe the same domain at different levels of detail \cite[p.\ 38]{euzenat_ontology_2013}.
%two ontologies describe different, possibly overlapping, domains at the same level of detail \cite[p.\ 38]{euzenat_ontology_2013}.
%
%semiotic heterogeneity: entities are interpreted differently by people \cite[p.\ 38]{euzenat_ontology_2013}.
%difficult for computer to detect and solve \cite[p.\ 38]{euzenat_ontology_2013}.

%\subsubsection{\normalfont {\textit{Token-based Distances}}}
%advantage:
%useful if people use very similar strings to denote the same concepts \cite[p.\ 95]{euzenat_ontology_2013}.
%
%disadvantage:
%synonyms with different structures will yield a low similarity \cite[p.\ 95]{euzenat_ontology_2013}.

%\subsubsection{Graph-based Matching Techniques}
%\begin{itemize}
%	\item $r$ comparing the entities in direct relation through $r$.
%	\item $r^{-}$ comparing the entities in the transitive reduction of relation $r$.
%	\item $r^{+}$ comparing the entities in the transitive closure of relation $r$.
%	\item $r^{-1}$ comparing the entities coming through a relation $r$.
%	\item $r^{\uparrow}$ comparing entities which are ultimately in $r^{+}$ (the maximal elements of the closure).	
%\end{itemize}
%===================================================
%\section{Data Integration}
%\label{sec:data-integration}
%definition:
%the practice associated with managing the data in motion: data that travels between applications, data stores, systems and organizations \cite[p.\ 7]{reeve_managing_2013}.
%
%purpose/usability/importance:
%managing data passing between systems and reducing the number and complexity of the interfaces between them \cite[p.\ 3]{reeve_managing_2013}.
%
%evolution:
%from point to point interface complexity to data integration strategy \cite[p.\ 11]{reeve_managing_2013}.
%integrate database (structured) data with data outside of databases (unstructured) \cite[p.\ 11]{reeve_managing_2013}.
%
%challenge:
%get the data to the right place, at the right time, and in the right format \cite[p.\ 7]{reeve_managing_2013}.
%additional information needs to be provided to look up how the source data should be transformed from one set of values to another \cite[p.\ 8]{reeve_managing_2013}.
%
%introduction:
%techniques, technologies, and best practices used for developing and managing data integration solutions \cite[p.\ 158]{reeve_managing_2013}.
%
%\section{Data Warehousing}
%\label{subsec:data-warehousing}
%definition:
%a set of materialized views over information sources, designed to provide an integrated and reconciled view of data of the organization \cite[p.\ 237]{calvanese_data_2001}.
%
%purpose/usability:
%information easily accessible.
%information present consistently.
%adaption to change.
%
%evolution:
%from point to point integration to integration using data warehouses \cite[p.\ 4]{kozielski_new_2009}.
%
%challenge:
%integrating structured, semi-structured, and unstructured data \cite[p.\ 2]{kozielski_new_2009}.
%warehousing imperfect data \cite[p.\ 2]{kozielski_new_2009}.
%\subsection{ETL and ELT}
%\label{subsec:etl-and-elt}
%definition:
%\ac{etl}
%extract data from internal and external sources, transform these data, load them into a data warehouse \cite[p.\ 285]{vaisman_data_2014}.
%
%evolution:
%data warehouses has been performed in an off-line fashion \cite[p.\ 19]{kozielski_new_2009}.
%\ac{etl} processes are evolving to real-time or near real-time \cite[p.\ 20]{kozielski_new_2009}.
%application light-weight transformation on-the-fly to minimize \cite[p.\ 20]{kozielski_new_2009}.
%
%example:
%Existing tools like Microsoft Integration Services and Pentaho Data Integration (aka Kettle) \cite[p.\ 285]{vaisman_data_2014}.
%
%disadvantage:
%complex and costly process, overcome by modeling \ac{etl} processes at conceptual level \cite[p.\ 285]{vaisman_data_2014}.
%no real time. data warehouse is typically updated every 24 hours \cite[p.\ 19]{kozielski_new_2009}.
%
%challenges:
%immediate propagation of the changes that take place at the sources was technically impossible, due to overhead \cite[p.\ 19]{kozielski_new_2009}.
%
%method:
%transformation from operational database and loading of the transformed data into the data warehouse \cite[p.\ 24]{kozielski_new_2009}.
%extraction of data from different, distributed, and often, heterogeneous data sources, their cleansing and customization in order to fit business needs and rules \cite[p.\ 21]{kozielski_new_2009}.
%transformation in order to fit the data warehouse schema \cite[p.\ 21]{kozielski_new_2009}.
%finally their loading into a data warehouse \cite[p.\ 21]{kozielski_new_2009}.
%
%elt:
%\ac{elt}
%first load the operational data directly in the data warehouse environment \cite[p.\ 24]{kozielski_new_2009}.
%then depending on the business needs, perform desired transformations rules \cite[p.\ 24]{kozielski_new_2009}.
%
%why elt:
%operational database machines do not have enough power \cite[p.\ 24]{kozielski_new_2009}.
%slow network connection among the data sources and the target warehouse \cite[p.\ 24]{kozielski_new_2009}.
%===================================================
%\section{Introduction to Data Mining}
%\label{sec:introduction-to-data-mining}
%definition:
%well-known scientific discipline
%tapping the data and extracting valuable information

%purpose/usability:
%a central part of knowledge discovery in database
%defined as the nontrivial process of identifying valid, novel, potentially useful, and ultimately understandable patterns in data\cite{fayyad_from_1996}
%find unsuspected relationships (models) and summarize (pattern) the data in novel ways

%evolution:
%secondary data analysis
%data have been collected for some purpose
%play no role in the data collection strategy

%challenge:
%housekeeping issues of how to store and access the data
%fundamental issues of how to determine the representativeness of the data

%application:
%classification, numerical, prediction, association and clustering
%successful; credit card fraud detection and medical diagnosis

%\section{Principles of Data Mining}
%\label{sec:principles-of-data-mining}
%introduction
%\subsection{Data Labeling}
%\label{subsec:data-labeling}
%definition:
%add informative meaning (labels) to a set of primary data
%primary data are

%purpose/procedure
%aim is assign labels and tags to data sets on the basis of its values
%data points with similar pattern are assigned the same labels\cite{che_unsupervised_2013}

%advantage:
%comprise the values of a number of variables
%find context between them

%disadvantage:
%labor-intensive, expensive and error-prone
%access to domain experts are limited
%concept drift
%\subsection{Data Preparation}
%\label{subsec:data-preparation}
%definition:
%convert real-world data to a computer readable format

%purpose:
%overcome errors because of wrong data entry or missing data or inconsistencies \cite{garcia_data_2015}

%procedure:
%data cleaning to filter and correct wrong data ending with consistent and almost error-free data set
%data normalization and transformation to generate new attributes with better properties (modeling variables or analytic variables)

%advantage:
%increase accuracy and efficiency
%\subsection{Data Reduction}
%\label{subsec:data-reduction}
%definition:
%reduce number of attributes or instances while maintaining the information as whole as possible
%also called dimension reduction\cite{garcia_data_2015, bramer_principles_2013}

%purpose/procedure:
%achieve a reduced representation of the data set
%smaller in volume keep most of the integrity of original data

%advantage:
%detect, remove or combine irrelevant dimensions
%irrelevant attributes place an unnecessary computational overhead\cite{garcia_data_2015, bramer_principles_2013}

%example:
%well known approaches are PCA, factor analysis, MDS and LLE\cite{garcia_data_2015}
%\subsection{Feature Selection}
%\label{subsec:feature-selection}
%definition:
%most commonly used techniques for dimensionality and data reduction
%a process to choose an optimal subset of features according to a certain criterion\cite{garcia_data_2015, bramer_principles_2013}

%purpose:
%reduced subset of features from the original set
%to improve performance or to visualize the data or to reduce dimensionality and remove noice\cite{garcia_data_2015}

%procedure:
%identify the features in the data set which are important
%discard others as redundant or irrelevant

%advantage:
%remove irrelevant data
%reduce cost of the data
%reduce complexity of the resulting model

%\subsection{Dealing with Missing Values}
%\label{subsec:dealing-with-missing-values}

%\subsection{Dealing with Noisy Data}
%\label{subsec:dealing-with-noisy-data}

%\subsection{Instance Selection}
%\label{subsec:instance-selection}

%figure description
%\begin{figure}[h]
%	\centering
%	\frame{\includegraphics{./figure/literature-review/layer-cake}}
%	\caption{Ontology Learning Layer Cake}
%	\label{fig:ontology-learning-layer-cake}
%\end{figure}
%terms; linguistic realizations of domain-specific concepts
%synonyms; finding words which denote the same concept
%concepts; a triple; an intensional definition of concepts, their extension and the lexical signs which are used to refer to them
%concept hierarchies; induce a concept hierarchy by learning pairs which forms a semi-upper lattice
%relations;

%\subsubsection{Probabilistic Matching Techniques}
%\label{subsubsec:probabilistic-matching-techniques}
%definition:
%probabilistic approaches matches a set of variables over which there is a probability distribution \cite[p.\ 183]{doan_principles_2012}

%disadvantage:
%long runtime than nonprobabilistic counterparts \cite[p.\ 183]{doan_principles_2012}
%hard to understand decisions of probabilistic approaches \cite[p.\ 183]{doan_principles_2012}
%\subsubsection{\normalfont {\textit{Expectation-Maximization (EM)}}}
%\label{subsubsec:expectation-maximization}
%definition:
%usage:
%example:
%equation:
%explanation:
%\cite[p.\ 187]{doan_principles_2012}